import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import HomeScreen from '../screens/Home'; 
import Details from '../screens/Details';
const Tab   = createBottomTabNavigator();
export default function Tabs(){
    return (
        <Tab.Navigator>
            <Tab.Screen options={{title:'Inicio'}}  name="Home" component={HomeScreen} />
            <Tab.Screen options={{title:'Detalles'}} name="Details" component={Details} />
        </Tab.Navigator>

    )
}


