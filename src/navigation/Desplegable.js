import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import HomeScreen from '../screens/Home'; 
import Details from '../screens/Details';
const Drawer = createDrawerNavigator();

export default function Desplegable(){
    return (
        <Drawer.Navigator initialRouteName="Home">
            <Drawer.Screen name="Home"          component={HomeScreen} />
            <Drawer.Screen name="Details"       component={Details} />
        </Drawer.Navigator>
    )
}


