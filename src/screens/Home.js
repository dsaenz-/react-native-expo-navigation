import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';

/*pantalla home*/
export default function Home( {navigation} ){
    return (
        <React.Fragment>
            <View style={styles.contenedorssss} >
                <Text>Home Screen</Text>
                <Button 
                        title="ir a detalles" 
                        onPress={ () => navigation.navigate('Details', 
                                                                {   
                                                                    id:93, 
                                                                    userName:'Elisa Lopez'
                                                                }
                                                            ) 
                                } 
                />
            </View>
        </React.Fragment>
    )
}


const styles = StyleSheet.create({
    contenedorssss: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
  });

